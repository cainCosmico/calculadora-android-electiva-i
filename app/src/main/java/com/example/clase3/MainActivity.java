package com.example.clase3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView panel;
    //private Button btnAbout;

    /*
    // Numbers
    private Button button0;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;
    private Button button7;
    private Button button8;
    private Button button9;

    // operators
    private Button button_punto;
    private Button button_igual;
    private Button button_mas;
    private Button button_menos;
    private Button button_div;
    private Button button_mul;
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        panel = findViewById(R.id.textView);
        panel.setText("");

        //btnAbout = findViewById(R.id.button_about);


        /*
        button0 = findViewById(R.id.button0);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        button5 = findViewById(R.id.button5);
        button6 = findViewById(R.id.button6);
        button7 = findViewById(R.id.button7);
        button8 = findViewById(R.id.button8);
        button9 = findViewById(R.id.button9);

        button_punto = findViewById(R.id.button_punto);
        button_igual = findViewById(R.id.button_igual);
        button_mas = findViewById(R.id.button_mas);
        button_menos = findViewById(R.id.button_res);
        button_div = findViewById(R.id.button_div);
        button_mul = findViewById(R.id.button_mul);
        */


    }

    public void setTextView(View view) {
        Button b = (Button) findViewById(view.getId());


        panel.append(b.getText());

        /*
        switch (view.getId()) {
            case R.id.button0: panel.append("0");
            case R.id.button1: panel.append("1");
            case R.id.button2: panel.append("2");
            case R.id.button3: panel.append("3");
            case R.id.button4: panel.append("4");
            case R.id.button5: panel.append("5");
            case R.id.button6: panel.append("6");
            case R.id.button7: panel.append("7");
            case R.id.button8: panel.append("8");
            case R.id.button9: panel.append("9");

            case R.id.button_punto: panel.append(".");
            case R.id.button_igual: panel.append("=");
            case R.id.button_mas: panel.append("+");
            case R.id.button_res: panel.append("-");
            case R.id.button_div: panel.append("/");
            case R.id.button_mul: panel.append("*");
        }
        https://www.antlr.org/
        spahs screem <- esto es lo que interesa
        crear una animacion con gif
        usar libreria implementacion



         */
    }

    public void calcularResultado(View view) {

    }

    public void buttonAboutAction(View view) {
        startActivity(new Intent(MainActivity.this, AboutActivity.class));


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) { // Horizontal
            //setContentView(R.layout.activity_main);
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) { // Vertical
            //setContentView(R.layout.activity_main);
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    * @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.yourxmlinlayout-land);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.yourxmlinlayoutfolder);
        }
    }
    *
    * */
}
